terraform {
  required_providers {
    aviatrix = {
      source  = "aviatrixsystems/aviatrix"
      version = "2.19.1"
    }
  }
  required_version = ">= 0.13"
}
