variable "aviatrix_username" { type = string }
variable "aviatrix_password" { type = string }
variable "aviatrix_controller" { type = string }

variable "region" {
    type = string
    description = "Region in which to deploy this environment"
    default = "eu-west-1"
}

variable "account" {
    type = string
    description = "AWS Account as known by the Aviatrix controller"
    default = "aws-account"
}