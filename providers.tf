provider "aviatrix" {
  controller_ip           = var.aviatrix_controller
  username                = var.aviatrix_username
  password                = var.aviatrix_password
  skip_version_validation = true
}