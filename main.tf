#Transit
module "aws-transit-example" {
  source  = "terraform-aviatrix-modules/aws-transit/aviatrix"
  version = "4.0.1"

  cidr    = "172.16.0.0/20"
  region  = var.region
  account = var.account
  ha_gw   = false
}

#Spoke
module "aws-spoke-example" {
  source  = "terraform-aviatrix-modules/aws-spoke/aviatrix"
  version = "4.0.1"

  name       = "aws-spoke-example"
  cidr       = "172.16.100.0/24"
  region     = var.region
  account    = var.account
  transit_gw = module.aws-transit-example.transit_gateway.gw_name
  ha_gw      = false
}
